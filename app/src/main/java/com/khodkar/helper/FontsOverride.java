package com.khodkar.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Hashtable;

public class FontsOverride {
    private static Hashtable<String, Typeface> fontCache = new Hashtable();



    public static void setFontTextView(TextView view, String font, Context context) {
        Typeface tf = getTypeface(font, context);
        if (tf != null) {
            view.setTypeface(tf);
        }
    }

    public static void setDefaultFont(Context context, String staticTypefaceFieldName, String fontAssetName) {

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontAssetName);
        replaceFont(staticTypefaceFieldName, typeface);
}

    private static void replaceFont(String staticTypefaceFieldName, Typeface newTypeface) {
        try {
            Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    public static Typeface getTypeface(String font, Context context) {
        Typeface tf = (Typeface) fontCache.get(font);
        if (tf == null) {
            try {
                String FONT_PERSIAN = "IRAN-Sans-Regular.ttf";
                if (font.equals(FONT_PERSIAN)) {
                    tf = Typeface.createFromAsset(context.getAssets(), FONT_PERSIAN);
                }
                fontCache.put(font, tf);
            } catch (Exception e) {
                return null;
            }
        }
        return tf;
    }
}
