package com.khodkar.helper;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v4.os.EnvironmentCompat;

public class PreferencesData {
    public static void saveString(Context context, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).commit();
    }

    public static void saveFloat(Context context, String key, Float value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat(key, value.floatValue()).commit();
    }

    public static void saveInt(Context context, String key, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).commit();
    }

    public static void saveLong(Context context, String key, long value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(key, value).apply();
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public static int getInt(Context context, String key, int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue);
    }

    public static String getString(Context context, String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
    }

    public static String getString(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, "");
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static float getFloat(Context context, String key, float defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(key, defaultValue);
    }

    public static long getLong(Context context, String key, long defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(key, defaultValue);
    }

    public static String returnUserId(Context context) {
        return getString(context, "userId", "0");
    }

    public static String returnServerTimeNewsComments(Context context) {
        return getString(context, "serverTimeNewsComments", "0");
    }

    public static String returnServerTimeNews(Context context) {
        return getString(context, "serverTimeNews", "0");
    }

    public static String returnServerTimeTopics(Context context) {
        return getString(context, "serverTimeTopics", "0");
    }

    public static String returnServerTimeGuest(Context context) {
        return getString(context, "serverTimeGuest", "0");
    }

    public static String returnServerTimeVideo(Context context) {
        return getString(context, "serverTimeVideo", "0");
    }

    public static String returnServerTimeTopVideo(Context context) {
        return getString(context, "serverTimeTopVideo", "0");
    }

    public static String returnServerTimeMyComments(Context context) {
        return getString(context, "serverTimeMyComments", "0");
    }

    public static String returnUserNumber(Context context) {
        return getString(context, "userNumber", EnvironmentCompat.MEDIA_UNKNOWN);
    }

    public static boolean returnIsSecondlyFragment(Context context) {
        return getBoolean(context, "secondlyFragment", false);
    }

    public static boolean returnIsPictureUpdate(Context context) {
        return getBoolean(context, "pictureUpdate", false);
    }

    public static String returnUserName(Context context) {
        return getString(context, "userName", EnvironmentCompat.MEDIA_UNKNOWN);
    }

    public static int returnTopicId(Context context) {
        return getInt(context, "topicId", 1);
    }

    public static int returnGuestId(Context context) {
        return getInt(context, "guestId", 1);
    }

    public static String returnWebserviceTime(Context context) {
        return getString(context, "webServiceTime", "0");
    }

    public static void saveWebserviceTime(Context context, String webServiceTime) {
        saveString(context, "webServiceTime", webServiceTime);
    }

    public static void saveUserId(Context context, String userId) {
        saveString(context, "userId", userId);
    }

    public static void saveUserNumber(Context context, String userNumber) {
        saveString(context, "userNumber", userNumber);
    }

    public static void saveUserName(Context context, String userName) {
        saveString(context, "userName", userName);
    }

    public static void saveServerTimeNews(Context context, String serverTime) {
        saveString(context, "serverTimeNews", serverTime);
    }

    public static void saveServerTimeNewsComments(Context context, String serverTime) {
        saveString(context, "serverTimeNewsComments", serverTime);
    }

    public static void saveServerTimeTopics(Context context, String serverTime) {
        saveString(context, "serverTimeTopics", serverTime);
    }

    public static void saveServerTimeGuest(Context context, String serverTime) {
        saveString(context, "serverTimeGuest", serverTime);
    }

    public static void saveServerTimeMyComments(Context context, String serverTime) {
        saveString(context, "serverTimeMyComments", serverTime);
    }

    public static void saveServerTimeVideo(Context context, String serverTime) {
        saveString(context, "serverTimeVideo", serverTime);
    }

    public static void saveServerTimeTopVideo(Context context, String serverTime) {
        saveString(context, "serverTimeTopVideo", serverTime);
    }

    public static void saveTopicId(Context context, int topicId) {
        saveInt(context, "topicId", topicId);
    }

    public static void saveGuestId(Context context, int guestId) {
        saveInt(context, "guestId", guestId);
    }

    public static void saveIsSecondlyFragment(Context context, boolean b) {
        saveBoolean(context, "secondlyFragment", b);
    }

    public static void saveIsPictureUpdate(Context context, boolean b) {
        saveBoolean(context, "pictureUpdate", b);
    }
}
