package com.khodkar.helper;

import android.os.Environment;

import java.io.File;

public class Statics {
    public static final int CACHE_DISK_USAGE_BYTES = 209715200;
    public static final int REQUEST_CODE_PERMISSION = 218;
    public static final String SHOP_DIRECTORY = (Environment.getExternalStorageDirectory() + File.separator + Utility.getContext());
    public static final String CACHE_DIRECTORY = (SHOP_DIRECTORY + File.separator + "cache");

    public static final String TAG = "HealthADebug";
    public static final String URL_IP = "";
    public static final String URL_PREFIX = "/api/mobile/";
    public static final int VOLLEY_AUTH_MAX_RETRIES = 1;
    public static final float VOLLEY_BACKOFF_MULT = 1.0f;
    public static final int VOLLEY_MAX_RETRIES = 3;
    public static final int VOLLEY_TIMEOUT_MS = 10000;
    public static String[] mPermission = new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.READ_PHONE_STATE", "android.permission.CAMERA"};
}
