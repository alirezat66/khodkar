package com.khodkar.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;

public class XmlToClassAttribHandler {
    private final String KEY_TEXT = "text";
    private final String KEY_TEXT_COLOR = "textColor";
    private final String KEY_TEXT_SIZE = "textSize";
    private AttributeSet mAttributeSet;
    private Context mContext;
    private Resources mRes;
    private String namespace = "http://noghteh.ir";

    public XmlToClassAttribHandler(Context context, AttributeSet attributeSet) {
        this.mContext = context;
        this.mRes = this.mContext.getResources();
        this.mAttributeSet = attributeSet;
    }

    public String getTextValue() {
        String value = this.mAttributeSet.getAttributeValue(this.namespace, "text");
        if (value == null) {
            return "";
        }
        if (value.length() > 1 && value.charAt(0) == '@' && value.contains("@string/")) {
            value = this.mRes.getString(this.mRes.getIdentifier(this.mContext.getPackageName() + ":" + value.substring(1), null, null));
        }
        return value;
    }

    public int getColorValue() {
        String value = this.mAttributeSet.getAttributeValue(this.namespace, "textColor");
        if (value == null) {
            return ViewCompat.MEASURED_STATE_MASK;
        }
        if (value.length() > 1 && value.charAt(0) == '@' && value.contains("@color/")) {
            return this.mRes.getColor(this.mRes.getIdentifier(this.mContext.getPackageName() + ":" + value.substring(1), null, null));
        }
        try {
            return Color.parseColor(value);
        } catch (Exception e) {
            return ViewCompat.MEASURED_STATE_MASK;
        }
    }

    public int getTextSize() {
        String value = this.mAttributeSet.getAttributeValue(this.namespace, "textSize");
        if (value == null) {
            return 12;
        }
        if (value.length() > 1 && value.charAt(0) == '@' && value.contains("@dimen/")) {
            return this.mRes.getDimensionPixelSize(this.mRes.getIdentifier(this.mContext.getPackageName() + ":" + value.substring(1), null, null));
        }
        try {
            return Integer.parseInt(value.substring(0, value.length() - 2));
        } catch (Exception e) {
            return 12;
        }
    }

    public int gettextSizeUnit() {
        String value = this.mAttributeSet.getAttributeValue(this.namespace, "textSize");
        if (value == null) {
            return 2;
        }
        try {
            String type = value.substring(value.length() - 2, value.length());
            if (type.equals("dp")) {
                return 1;
            }
            if (type.equals("sp")) {
                return 2;
            }
            if (type.equals("pt")) {
                return 3;
            }
            if (type.equals("mm")) {
                return 5;
            }
            if (type.equals("in")) {
                return 4;
            }
            if (type.equals("px")) {
                return 0;
            }
            return -1;
        } catch (Exception e) {
            return -1;
        }
    }
}
