package com.khodkar.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

public class FontChange {
    public static ArrayList<Button> getButtons(ViewGroup root) {
        ArrayList<Button> views = new ArrayList();
        for (int i = 0; i < root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if (v instanceof Button) {
                views.add((Button) v);
            } else if (v instanceof ViewGroup) {
                views.addAll(getButtons((ViewGroup) v));
            }
        }
        return views;
    }

    public static ArrayList<EditText> getEdittext(ViewGroup root) {
        ArrayList<EditText> views = new ArrayList();
        for (int i = 0; i < root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if (v instanceof EditText) {
                views.add((EditText) v);
            } else if (v instanceof ViewGroup) {
                views.addAll(getEdittext((ViewGroup) v));
            }
        }
        return views;
    }

    public static ArrayList<TextView> getTextViews(ViewGroup root) {
        ArrayList<TextView> views = new ArrayList();
        for (int i = 0; i < root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if (v instanceof TextView) {
                views.add((TextView) v);
            } else if (v instanceof ViewGroup) {
                views.addAll(getTextViews((ViewGroup) v));
            }
        }
        return views;
    }

    public static ArrayList<MenuItem> getMenuItems(ViewGroup root) {
        ArrayList<MenuItem> views = new ArrayList();
        for (int i = 0; i < root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if (v instanceof MenuItem) {
                views.add((MenuItem) v);
            } else if (v instanceof ViewGroup) {
                views.addAll(getMenuItems((ViewGroup) v));
            }
        }
        return views;
    }



    public static void setEditTextFont(Context context, EditText... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (EditText edt : params) {
            edt.setTypeface(tf);
        }
    }

    public static void setLayoutFont(Context context, TextView... params) {
    }

    public static void setButtonFont(Context context, Button... Params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (Button bt : Params) {
            bt.setTypeface(tf);
        }
    }

    private void setFontOnPicker(LinearLayout ll, Context context) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
    }

    public static void setTextViewFont(Context context, TextView... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (TextView tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setTextViewPlateFont(Context context, TextView... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (TextView tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setTextViewIRFont(Context context, TextView... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (TextView tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setTextViewFontTitle(Context context, TextView... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (TextView tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setMaskedTextViewFont(Context context, EditText... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (EditText tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setMaskedTextViewFont(Context context, TextView... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (TextView tv : params) {
            tv.setTypeface(tf);
        }
    }

    public static void setRadioButtonFont(Context context, RadioButton... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (RadioButton rb : params) {
            rb.setTypeface(tf);
        }
    }

    public static void setCheckboxFont(Context context, CheckBox... params) {
        Typeface tf = Typeface.createFromAsset(context.getAssets(), Language(context));
        for (CheckBox ch : params) {
            ch.setTypeface(tf);
        }
    }


    public static String Language(Context context) {
        return "IRAN-Sans-Medium.ttf";
    }

    public static String Language2(Context context) {
        return "IRAN-Sans-Regular.ttf";
    }

    public void ChangeFont(Context context, ViewGroup root) {
        int i;
        ArrayList<EditText> alledts = getEdittext(root);
        ArrayList<Button> allbtns = getButtons(root);
        ArrayList<TextView> alltxts = getTextViews(root);
        ArrayList<MenuItem> menuItems = getMenuItems(root);
        for (i = 0; i < alledts.size(); i++) {
            setEditTextFont(context, (EditText) alledts.get(i));
        }
        for (i = 0; i < allbtns.size(); i++) {
            setButtonFont(context, (Button) allbtns.get(i));
        }
        for (i = 0; i < alltxts.size(); i++) {
            setTextViewFont(context, (TextView) alltxts.get(i));
        }

    }
}
