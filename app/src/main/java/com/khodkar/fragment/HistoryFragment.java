package com.khodkar.fragment;

import android.app.Fragment;
import android.app.FragmentContainer;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.khodkar.Controller.AppController;
import com.khodkar.R;
import com.khodkar.adapter.ServicesListAdapter;
import com.khodkar.objects.MyServices;

import java.util.ArrayList;

/**
 * Created by alireza on 7/9/2017.
 */

public class HistoryFragment extends android.support.v4.app.Fragment{
    View view;
    Context context;
    boolean flag=false;
    ArrayList<MyServices> servicesList = new ArrayList<>();
    ServicesListAdapter adapter;
    ListView list;
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_myservices, container, false);
        context = AppController.getCurrentContext();
        makeView();

        return view;
    }

    private void makeView() {

        list = (ListView) view.findViewById(R.id.list);
        MyServices mys = new MyServices("دبستان پسرانه گلهای نبوت");
        MyServices mys2 = new MyServices("پیش دبستان دخترانه نیلوفر");
        servicesList.add(mys);
        servicesList.add(mys2);
        adapter = new ServicesListAdapter(getActivity(),servicesList);
        list.setAdapter(adapter);


        ///define elements

        ///
    }
}
