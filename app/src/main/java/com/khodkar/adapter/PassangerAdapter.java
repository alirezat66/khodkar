package com.khodkar.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.khodkar.R;
import com.khodkar.objects.Passanger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alireza on 6/17/2017.
 */

public class PassangerAdapter extends RecyclerView.Adapter<PassangerAdapter.MyViewHolder> {
    private ArrayList<Passanger> list;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txtAdd;
        LinearLayout lyIn,lyOut,lyCall,lyMessage;
        RelativeLayout lyContact;
        ImageView imgOut,imgIn;

        public MyViewHolder(View view) {
            super(view);
            txtName = (TextView) view.findViewById(R.id.txtName);
            txtAdd = (TextView) view.findViewById(R.id.txtAddress);
            lyIn = (LinearLayout) view.findViewById(R.id.lyin);
            lyOut = (LinearLayout) view.findViewById(R.id.lyout);
            lyCall = (LinearLayout) view.findViewById(R.id.lycall);
            lyMessage = (LinearLayout) view.findViewById(R.id.lymessage);
            lyContact = (RelativeLayout) view.findViewById(R.id.lycontact);

            imgOut = (ImageView) view.findViewById(R.id.imgout);
            imgIn = (ImageView) view.findViewById(R.id.imgin);

        }
    }
    public PassangerAdapter(ArrayList<Passanger>list,Context context){
        this.list=list;
        this.context=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_passanger, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Passanger item = list.get(position);
        holder.txtName.setText(item.getName());
        holder.txtAdd.setText(item.getAddress());
        if(item.isOpenContact()){
            holder.lyContact.setVisibility(View.VISIBLE);
        }else {
            holder.lyContact.setVisibility(View.GONE);
        }

        if(item.isInEnter()){
            holder.imgIn.setImageResource(R.drawable.ic_enter_active);
        }else {
            holder.imgIn.setImageResource(R.drawable.ic_enter);
        }

        if(item.isExit()){
            holder.imgOut.setImageResource(R.drawable.ic_exit_active);
        }else {
            holder.imgOut.setImageResource(R.drawable.ic_exit);
        }

        holder.lyCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0 ; i <list.size();i++){
                    list.get(i).setOpenContact(false);
                }
                item.setOpenContact(true);
                notifyDataSetChanged();
            }
        });
        holder.lyMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0 ; i <list.size();i++){
                    list.get(i).setOpenContact(false);
                }
                item.setOpenContact(true);
                notifyDataSetChanged();
            }
        });
        holder.lyOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setExit(!item.isExit());
                notifyDataSetChanged();
            }
        });
        holder.lyIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setInEnter(!item.isInEnter());
              notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
