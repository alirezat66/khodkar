package com.khodkar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.khodkar.R;
import com.khodkar.objects.ConversationResponseData;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

/**
 * Created by alireza on 3/30/2017.
 */

public class ConversationListAdapter extends BaseAdapter {


        public List<ConversationResponseData> listItems;
        Activity context;

    private static final long NOW = new Date().getTime();

    View parentView;
        AdapterView.OnItemClickListener onItemClickListener;
        public boolean flagisupdate = false;
        public int positionItem;
        public int itemId;

        public ConversationListAdapter(Activity context/*, AdapterView.OnItemClickListener onItemClickListener*/, List<ConversationResponseData> list) {
            this.listItems = list;
            this.context = context;
            this.onItemClickListener = onItemClickListener;
        }


        @Override
        public int getCount() {
            return listItems.size();
        }

        @Override
        public ConversationResponseData getItem(int position) {
            return listItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            public TextView txtTitle,txtDes,txtCounter;
            ShapedImageView img;

        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            LayoutInflater inflater = context.getLayoutInflater();
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_conversation, null);
                holder = new ViewHolder();
               holder.txtTitle      = (TextView)convertView.findViewById(R.id.txt_title);
               holder.txtDes = (TextView)convertView.findViewById(R.id.txt_desc);
               holder.txtCounter = (TextView)convertView.findViewById(R.id.txt_counter);
               holder.img = (ShapedImageView)convertView.findViewById(R.id.imgicon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


             ConversationResponseData data = listItems.get(position);

            holder.txtTitle.setText(data.getFname()+" "+data.getLname());

            String res = "";
            holder.txtTitle.setText(data.getFname()+" "+data.getLname());
            holder.txtDes.setText(data.getState());
            /*Picasso.with(context)
                    .load(s)
                    .placeholder(errorid)
                    .error(errorid)
                    .into(holder.img);*/
            if(data.getUnseen().equals("0")){
                holder.txtCounter.setVisibility(View.INVISIBLE);
            }else {
                holder.txtCounter.setText(data.getUnseen());
            }
            return convertView;
        }
}
