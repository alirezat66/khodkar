package com.khodkar.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.khodkar.R;
import com.khodkar.helper.PreferencesData;
import com.khodkar.objects.PrivateMessageDataBase;
import com.khodkar.views.CustomCurveButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;


/**
 * Created by alireza on 3/30/2017.
 */

public class HistoryAdapter extends BaseAdapter  {


    public List<PrivateMessageDataBase> listItems;
    Activity context;
    View parentView;
    AdapterView.OnItemClickListener onItemClickListener;
    public boolean flagisupdate = false;
    public int positionItem;
    public int itemId;

    public HistoryAdapter(Activity context/*, AdapterView.OnItemClickListener onItemClickListener*/, List<PrivateMessageDataBase> list) {
        this.listItems = list;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }


    public void add(PrivateMessageDataBase hd) {
        listItems.add(hd);
        notifyDataSetChanged();
    }
    public void clear(){
        listItems.clear();
    }
    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public PrivateMessageDataBase getItem(int position) {
        return listItems.get(position);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }



    private class ViewHolder {
        public String id;
        public TextView txtTime,txtText;
        public ImageView img_history,btn_play,btn_down;
        public  ImageView img_state;
        public CustomCurveButton btnDate;
/*
        ImageView img;
*/

    }
    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final HistoryAdapter.ViewHolder holder;
        final PrivateMessageDataBase data = listItems.get(position);

        if (convertView == null || !((ViewHolder)convertView.getTag()).id.equals(this.listItems.get(position).getId()) ) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (data.getMtype().equals("me")) {
                convertView = vi.inflate(R.layout.item_message_my, null);
            } else if (data.getMtype().equals("you")) {
                convertView = vi.inflate(R.layout.item_message_other, null);
            } else {
                convertView = vi.inflate(R.layout.item_message_break, null);

            }

            holder = new HistoryAdapter.ViewHolder();
            holder.id = listItems.get(position).getId()+"";




            if(data.getMtype().equals("me")||data.getMtype().equals("you")){
                holder.txtText  = (TextView) convertView.findViewById(R.id.txtMessage);
            }else {
                    holder.btnDate  = (CustomCurveButton) convertView.findViewById(R.id.txtDestance);
            }


            convertView.setTag(holder);
        } else {
            holder = (HistoryAdapter.ViewHolder) convertView.getTag();

        }


        if(holder.txtText!=null){
            holder.txtText.setText(data.getText());
        }
        if(holder.btnDate!=null){

        }
        return convertView;
    }


}



