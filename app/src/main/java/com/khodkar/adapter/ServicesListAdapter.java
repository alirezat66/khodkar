package com.khodkar.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.khodkar.R;
import com.khodkar.activities.DetailActivity;
import com.khodkar.activities.PassangerActivity;
import com.khodkar.objects.ConversationResponseData;
import com.khodkar.objects.MyServices;

import java.util.Date;
import java.util.List;

import cn.gavinliu.android.lib.shapedimageview.ShapedImageView;

/**
 * Created by alireza on 3/30/2017.
 */

public class ServicesListAdapter extends BaseAdapter {


        public List<MyServices> listItems;
        Activity context;

    private static final long NOW = new Date().getTime();

    View parentView;
        AdapterView.OnItemClickListener onItemClickListener;
        public boolean flagisupdate = false;
        public int positionItem;
        public int itemId;

        public ServicesListAdapter(Activity context/*, AdapterView.OnItemClickListener onItemClickListener*/, List<MyServices> list) {
            this.listItems = list;
            this.context = context;
            this.onItemClickListener = onItemClickListener;
        }


        @Override
        public int getCount() {
            return listItems.size();
        }

        @Override
        public MyServices getItem(int position) {
            return listItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            public TextView txtTitle;
            TextView btnInfo,btnDeteil;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            LayoutInflater inflater = context.getLayoutInflater();
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_service, null);
                holder = new ViewHolder();
               holder.txtTitle      = (TextView)convertView.findViewById(R.id.txtName);

                holder.btnInfo  = (TextView) convertView.findViewById(R.id.txttinfo);
                holder.btnDeteil  = (TextView) convertView.findViewById(R.id.txtdetail);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


             MyServices data = listItems.get(position);

            holder.txtTitle.setText(data.getSchoolName());
            holder.btnDeteil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PassangerActivity.class);
                    context.startActivity(intent);

                }
            });
            holder.btnInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    context.startActivity(intent);
                }
            });

            return convertView;
        }
}
