package com.khodkar.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apg.mobile.roundtextview.RoundTextView;
import com.khodkar.R;
import com.khodkar.objects.Passanger;
import com.khodkar.objects.SettingObject;

import java.util.ArrayList;

/**
 * Created by alireza on 6/17/2017.
 */

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder> {
    private ArrayList<SettingObject> list;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle, txtDesc;
        ImageView imgIcon;
        RoundTextView numberTxt;

        public MyViewHolder(View view) {
            super(view);
            txtTitle = (TextView) view.findViewById(R.id.title);
            txtDesc = (TextView) view.findViewById(R.id.description);
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            numberTxt = (RoundTextView) view.findViewById(R.id.txt_counter);

        }
    }
    public SettingAdapter(ArrayList<SettingObject>list, Context context){
        this.list=list;
        this.context=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_setting, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SettingObject item = list.get(position);
        holder.txtTitle.setText(item.getTitle());
        holder.txtDesc.setText(item.getDesc());
        if(item.getNumber()==0){
            holder.numberTxt.setVisibility(View.GONE);
        }
        holder.imgIcon.setImageResource(item.getPicId());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
