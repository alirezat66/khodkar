package com.khodkar.mapServices;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khodkar.R;
import com.khodkar.objects.Passanger;

import java.util.ArrayList;

public class ViewPagerAdapter extends PagerAdapter {
    // Declare Variables
    Context context;


    ArrayList<Passanger> array = new ArrayList<Passanger>();
    LayoutInflater inflater;

    public ViewPagerAdapter(Context context, ArrayList<Passanger> arraytemp) {
        this.context = context;
        array = arraytemp;
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


         TextView txtName, txtAdd;
        LinearLayout lyIn,lyOut,lyCall,lyMessage;
        RelativeLayout lyContact;
        ImageView imgOut,imgIn;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_passanger, container, false);


        txtName = (TextView) view.findViewById(R.id.txtName);
        txtAdd = (TextView) view.findViewById(R.id.txtAddress);
        lyIn = (LinearLayout) view.findViewById(R.id.lyin);
        lyOut = (LinearLayout) view.findViewById(R.id.lyout);
        lyCall = (LinearLayout) view.findViewById(R.id.lycall);
        lyMessage = (LinearLayout) view.findViewById(R.id.lymessage);
        lyContact = (RelativeLayout) view.findViewById(R.id.lycontact);

        imgOut = (ImageView) view.findViewById(R.id.imgout);
        imgIn = (ImageView) view.findViewById(R.id.imgin);

        final Passanger item = array.get(position);
        txtName.setText(item.getName());
        txtAdd.setText(item.getAddress());
        if(item.isOpenContact()){
           lyContact.setVisibility(View.VISIBLE);
        }else {
           lyContact.setVisibility(View.GONE);
        }

        if(item.isInEnter()){
           imgIn.setImageResource(R.drawable.ic_enter_active);
        }else {
            imgIn.setImageResource(R.drawable.ic_enter);
        }

        if(item.isExit()){
           imgOut.setImageResource(R.drawable.ic_exit_active);
        }else {
            imgOut.setImageResource(R.drawable.ic_exit);
        }

        lyCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0 ; i <array.size();i++){
                    array.get(i).setOpenContact(false);
                }
                item.setOpenContact(true);
                notifyDataSetChanged();
            }
        });
        lyMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0 ; i <array.size();i++){
                    array.get(i).setOpenContact(false);
                }
                item.setOpenContact(true);
                notifyDataSetChanged();
            }
        });
        lyOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setExit(!item.isExit());
                notifyDataSetChanged();
            }
        });
        lyIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setInEnter(!item.isInEnter());
                notifyDataSetChanged();
            }
        });

        ((ViewPager) container).addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((CardView) object);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
