package com.khodkar.mapServices;


import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.khodkar.R;
import com.khodkar.objects.Passanger;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class Slider {


    ArrayList<Passanger> list = new ArrayList<Passanger>();
    ViewPager viewPager;
    CircleIndicator indicator;
    LinearLayout llDots;
    RelativeLayout windowSlider;

    int onscreen = 0;


    Activity _activity;


    public Slider(Activity activity, ArrayList<Passanger> result) {

        _activity = activity;
        indicator = (CircleIndicator) activity.findViewById(R.id.indicator);

        viewPager = (ViewPager) activity.findViewById(R.id.viewpager);


        list = result;

    }



    public void run( ){

        adaptView();

    }

    private void adaptView() {

        //set on change
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                onscreen = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //adapt slider
        ViewPagerAdapter adapter = new ViewPagerAdapter(_activity, list);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
    }









}