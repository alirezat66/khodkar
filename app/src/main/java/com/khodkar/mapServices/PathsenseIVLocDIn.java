package com.khodkar.mapServices;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.pathsense.android.sdk.location.PathsenseInVehicleLocation;
import com.pathsense.android.sdk.location.PathsenseInVehicleLocationUpdateReceiver;

/**
 * Created by alireza on 6/16/2017.
 */

public class PathsenseIVLocDIn extends PathsenseInVehicleLocationUpdateReceiver
{
    static final String TAG = PathsenseIVLocDIn.class.getName();
    @Override
    protected void onInVehicleLocationUpdate(Context context, PathsenseInVehicleLocation pathsenseInVehicleLocation)
    {
        Log.i(TAG, "inVehicleLocation=" + pathsenseInVehicleLocation);
        // broadcast in-vehicle location update
        Intent inVehicleLocationUpdateIntent = new Intent("inVehicleLocationUpdate");
        inVehicleLocationUpdateIntent.putExtra("inVehicleLocation", pathsenseInVehicleLocation);
        LocalBroadcastManager.getInstance(context).sendBroadcast(inVehicleLocationUpdateIntent);
    }
}