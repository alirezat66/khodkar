package com.khodkar.mapServices;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.khodkar.Controller.AppController;
import com.khodkar.EventBus.MessengerBus;
import com.pathsense.android.sdk.location.PathsenseLocationProviderApi;

import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

/**
 * Created by alireza on 6/16/2017.
 */

public class mySenderTwo extends Service {
    private Timer timer = new Timer();

    static class InternalGroundTruthLocationListener implements LocationListener {
        mySenderTwo mService;

        //
        InternalGroundTruthLocationListener(mySenderTwo service) {
            mService = service;
        }

        @Override
        public void onLocationChanged(Location location) {
            final mySenderTwo service = mService;
            //
            if (service != null) {
                if (location != null) {




                    MessengerBus eb = new MessengerBus("locchange", location);
                    EventBus bus = EventBus.getDefault();
                    bus.post(eb);
                    Log.i(TAG, "groundTruthLocation=" + location);
                    // broadcast ground truth location update
                    Intent groundTruthLocationUpdateIntent = new Intent("groundTruthLocationUpdate");
                    groundTruthLocationUpdateIntent.putExtra("groundTruthLocation", location);
                    LocalBroadcastManager.getInstance(service).sendBroadcast(groundTruthLocationUpdateIntent);
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    //
    static final String TAG = PathsenseIVLocDIn.class.getName();
    //
    InternalGroundTruthLocationListener mGroundTruthLocationListener;
    LocationManager mLocationManager;

    //
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e("services", "service runed");
        final LocationManager locationManager = mLocationManager;
        //
        if (locationManager != null) {
            String action = intent != null ? intent.getAction() : null;
            if ("start".equals(action))

            {
                ReapetCheck();
                Log.e("test:", "start");

                // register for ground truth location updates
                if (mGroundTruthLocationListener == null) {
                    mGroundTruthLocationListener = new InternalGroundTruthLocationListener(this);
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                     // TODO: 6/20/2017  ;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mGroundTruthLocationListener);
                // request in-vehicle location updates
                PathsenseLocationProviderApi.getInstance(this).requestInVehicleLocationUpdates(PathsenseIVLocDIn.class);
            } else if ("stop".equals(action))
            {
                Log.e("test:","stop");
                // unregister for ground truth location updates
                if (mGroundTruthLocationListener != null)
                {
                    locationManager.removeUpdates(mGroundTruthLocationListener);
                }
                // remove in-vehicle location updates
                PathsenseLocationProviderApi.getInstance(this).removeInVehicleLocationUpdates();
                stopSelf();
            }
        }
        return START_STICKY;
    }


    private void ReapetCheck() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {


                Log.e("test:","khodahafez");
            }
        }, 0, 10000);
    }

}