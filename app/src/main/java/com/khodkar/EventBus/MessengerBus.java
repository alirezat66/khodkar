package com.khodkar.EventBus;


import android.location.Location;

public class MessengerBus {

    String message,value;
    String eId,userId;
    int missionId;
    Location location;

    public MessengerBus() {
    }

    public MessengerBus(String message) {

        this.message = message;

    }


    public MessengerBus(String message, String value) {
        this.message = message;
        this.value = value;
    }
    public MessengerBus(String message, Location location){
        this.message=message;
        this.location=location;
    }
    public MessengerBus(String message, int missionId) {
        this.message = message;
        this.missionId = missionId;
    }

    public Location getLocation() {
        return location;
    }

    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String geteId() {
        return eId;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return this.message;
    }
    public MessengerBus(String name, String eId, String userId){
        this.message = name;
        this.eId =eId;
        this.userId = userId;

    }

}
