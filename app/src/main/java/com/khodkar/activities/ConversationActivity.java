package com.khodkar.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.khodkar.R;
import com.khodkar.adapter.ConversationListAdapter;
import com.khodkar.adapter.PassangerAdapter;
import com.khodkar.objects.ConversationResponseData;
import com.khodkar.objects.Passanger;

import java.util.ArrayList;

/**
 * Created by alireza on 6/17/2017.
 */

public class ConversationActivity extends AppCompatActivity {
    ListView list;
    ConversationListAdapter adapter;
    ArrayList<ConversationResponseData>convlist=new ArrayList<>();
    Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversations);
        context=this;
        makePage();
    }

    private void makePage() {
        list = (ListView) findViewById(R.id.list);
        convlist.add(new ConversationResponseData("حسین","پورتوکل","","مدیریت","3"));
        convlist.add(new ConversationResponseData("حسین","پورتوکل","","مدیریت","0"));
        convlist.add(new ConversationResponseData("حسین","پورتوکل","","راننده سرویس","1"));
        convlist.add(new ConversationResponseData("مدرسه","گلهای نبوت","","مدیریت","0"));
        convlist.add(new ConversationResponseData("حسین","پورتوکل","","پشتیبان ولایت","2"));


        adapter = new ConversationListAdapter( ConversationActivity.this,convlist);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(context,ChatingActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
