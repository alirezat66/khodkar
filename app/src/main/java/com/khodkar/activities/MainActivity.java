package com.khodkar.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arsy.maps_library.MapRipple;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.khodkar.EventBus.MessengerBus;
import com.khodkar.R;
import com.khodkar.mapServices.FusedLocationManager;
import com.khodkar.mapServices.mySenderService;
import com.khodkar.mapServices.mySenderTwo;
import com.khodkar.mapServices.Slider;
import com.khodkar.objects.Passanger;
import com.khodkar.retrofit.ServerListener;
import com.khodkar.retrofit.callerService;
import com.pathsense.android.sdk.location.PathsenseInVehicleLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import de.greenrobot.event.EventBus;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback ,LocationListener ,View.OnClickListener,ServerListener{
    MapRipple mapRipple;
    EventBus bus = EventBus.getDefault();
    ImageView imgNavigation,btnPassanger,btnHide,btnChat,btnRoute,btnSetting;
    Context context;
    LinearLayout sliderview;
    CardView dialogNavigation;
    ArrayList<Passanger>passangerlist=new ArrayList<>();
    ServerListener sl;
    ViewPager viewPager ;



    Slider sliderobj;


    long counter=0;
    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId()){

            case R.id.btnChat:
                dialogNavigation.setVisibility(View.GONE);
                 intent= new Intent(context,ConversationActivity.class);
                startActivity(intent);
                break;
            case R.id.btnNavigation:
                dialogNavigation.setVisibility(View.VISIBLE);
                break;
            case R.id.btnPassanger:
                dialogNavigation.setVisibility(View.GONE);
                 intent = new Intent(context,PassangerActivity.class);
                startActivity(intent);
                break;
            case R.id.btnRoute:
                dialogNavigation.setVisibility(View.GONE);
                intent = new Intent(context,ServiceActivity.class);
                startActivity(intent);
                break;
            case R.id.btnSetting:
                dialogNavigation.setVisibility(View.GONE);
                intent = new Intent(context,SettingActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onFailure(int i, String str) {

    }

    @Override
    public void onSuccess(int i, JsonObject jsonObject) {

    }

    static class InternalGroundTruthLocationUpdateReceiver extends BroadcastReceiver
    {
        MainActivity mActivity;
        //
        InternalGroundTruthLocationUpdateReceiver(MainActivity activity)
        {
            mActivity = activity;
        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final MainActivity activity = mActivity;
            final InternalHandler handler = activity != null ? activity.mHandler : null;

            //
            if (activity != null && handler != null)
            {
                Location groundTruthLocation = intent.getParcelableExtra("groundTruthLocation");
                Message msg = Message.obtain();
                msg.what = MESSAGE_ON_GROUND_TRUTH_LOCATION;
                msg.obj = groundTruthLocation;
                handler.sendMessage(msg);
            }
        }
    }
    static class InternalHandler extends Handler
    {
        MainActivity mActivity;
        //
        InternalHandler(MainActivity activity)
        {
            mActivity = activity;
        }
        @Override
        public void handleMessage(Message msg)
        {
            final MainActivity activity = mActivity;
            final GoogleMap map = activity != null ? activity.mMap : null;
            //
            if (activity != null && map != null)
            {
                switch (msg.what)
                {
                    case MESSAGE_ON_IN_VEHICLE_LOCATION_UPDATE:
                    {
                        PathsenseInVehicleLocation inVehicleLocation = (PathsenseInVehicleLocation) msg.obj;
                        LatLng position = new LatLng(inVehicleLocation.getLatitude(), inVehicleLocation.getLongitude());
                        Marker markerInVehicle = activity.mMarkerInVehicle;
                        if (markerInVehicle == null)
                        {
                            markerInVehicle = map.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location_icon)).anchor(.5f, .5f).position(position).title("PathsenseInVehicle"));
                            activity.mMarkerInVehicle = markerInVehicle;
                        } else
                        {
                            markerInVehicle.setPosition(position);
                        }
                        activity.mHeadingRoad = activity.unwrapHeading(inVehicleLocation.getBearing(), activity.mHeadingGroundTruth);
                        markerInVehicle.setRotation((float) activity.mHeadingRoad + 90);
                        map.moveCamera(CameraUpdateFactory.newLatLng(position));
                        activity.drawPolylineInVehicle(inVehicleLocation);
                        break;
                    }
                    case MESSAGE_ON_GROUND_TRUTH_LOCATION:
                    {
                        final Marker groundTruthMarker = activity.mMarkerGroundTruth;
                        //
                        if (groundTruthMarker != null)
                        {
                            Location groundTruthLocation = (Location) msg.obj;
                            float bearing = groundTruthLocation.getBearing();
                            if (bearing != 0)
                            {
                                activity.mHeadingGroundTruth = groundTruthLocation.getBearing();
                            }
                            groundTruthMarker.setRotation((float) activity.mHeadingGroundTruth - 90);
                            LatLng position = new LatLng(groundTruthLocation.getLatitude(), groundTruthLocation.getLongitude());
                            groundTruthMarker.setPosition(position);
                            //map.moveCamera(CameraUpdateFactory.newLatLng(position));
                            activity.drawPolylineGroundTruth(groundTruthLocation);
                        }
                        break;
                    }
                }
            }
        }
    }
    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    static class InternalInVehicleLocationUpdateReceiver extends BroadcastReceiver
    {
        MainActivity mActivity;
        ServerListener sl;
        //
        InternalInVehicleLocationUpdateReceiver(MainActivity activity)
        {
            mActivity = activity;
        }
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final MainActivity activity = mActivity;
            final InternalHandler handler = activity != null ? activity.mHandler : null;
            //
            if (activity != null && handler != null)
            {
                PathsenseInVehicleLocation inVehicleLocationUpdate = intent.getParcelableExtra("inVehicleLocation");
                Message msg = Message.obtain();
                msg.what = MESSAGE_ON_IN_VEHICLE_LOCATION_UPDATE;
                msg.obj = inVehicleLocationUpdate;
                handler.sendMessage(msg);
            }
        }
    }
    //
    static final String TAG = MainActivity.class.getName();
    // Messages
    static final int MESSAGE_ON_IN_VEHICLE_LOCATION_UPDATE = 0;
    static final int MESSAGE_ON_GROUND_TRUTH_LOCATION = 1;
    //
    double mHeadingGroundTruth;
    double mHeadingRoad;
    int mCreateFlag;
    GoogleMap mMap;
    InternalGroundTruthLocationUpdateReceiver mGroundTruthLocationUpdateReceiver;
    InternalHandler mHandler = new InternalHandler(this);
    InternalInVehicleLocationUpdateReceiver mInVehicleLocationUpdateReceiver;
    List<Circle> mInVehiclePointMarkers = new ArrayList<Circle>();
    List<Location> mGroundTruthLocations = new ArrayList<Location>();
    List<PathsenseInVehicleLocation> mInVehicleLocations = new ArrayList<PathsenseInVehicleLocation>();
    Marker mMarkerGroundTruth;
    Marker mMarkerInVehicle;
    Polyline mPolylineGroundTruth;
    Polyline mPolylineInVehicle;
    SharedPreferences mPreferences;
    //
    void drawPolylineGroundTruth(Location groundTruthLocation)
    {
        final List<Location> groundTruthLocations = mGroundTruthLocations;
        final GoogleMap map = mMap;
        //
        if (groundTruthLocations != null && map != null)
        {
            int numGroundTruthLocations = groundTruthLocations.size();
            if (numGroundTruthLocations > 0)
            {
                long timestamp = System.currentTimeMillis();
                for (int i = numGroundTruthLocations - 1; i > -1; i--)
                {
                    Location q_groundTruthLocation = groundTruthLocations.get(i);
                    if ((timestamp - q_groundTruthLocation.getTime()) > 60000)
                    {
                        groundTruthLocations.remove(i);
                        numGroundTruthLocations--;
                    }
                }
            }
            groundTruthLocations.add(0, groundTruthLocation);
            numGroundTruthLocations++;
            //
            if (numGroundTruthLocations > 1)
            {
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.width(15);
                polylineOptions.color(Color.RED);
                polylineOptions.geodesic(true);
                for (int i = 0; i < numGroundTruthLocations; i++)
                {
                    Location q_groundTruthLocation = groundTruthLocations.get(i);
                    polylineOptions.add(new LatLng(q_groundTruthLocation.getLatitude(), q_groundTruthLocation.getLongitude()));
                }
                if (mPolylineGroundTruth != null)
                {
                    mPolylineGroundTruth.remove();
                }
                mPolylineGroundTruth = map.addPolyline(polylineOptions);
            }
        }
    }
    void drawPolylineInVehicle(PathsenseInVehicleLocation inVehicleLocation)
    {
        final List<PathsenseInVehicleLocation> inVehicleLocations = mInVehicleLocations;
        final List<Circle> inVehiclePointMarkers = mInVehiclePointMarkers;
        final GoogleMap map = mMap;
        //
        if (inVehicleLocations != null && inVehiclePointMarkers != null && map != null)
        {
            int numInVehicleLocations = inVehicleLocations.size();
            if (numInVehicleLocations > 0)
            {
                long timestamp = System.currentTimeMillis();
                for (int i = numInVehicleLocations - 1; i > -1; i--)
                {
                    PathsenseInVehicleLocation q_inVehicleLocation = inVehicleLocations.get(i);
                    if ((timestamp - q_inVehicleLocation.getTime()) > 60000)
                    {
                        inVehicleLocations.remove(i);
                    }
                }
            }
            int numInVehiclePointMarkers = inVehiclePointMarkers.size();
            if (numInVehiclePointMarkers > 0)
            {
                for (int i = numInVehiclePointMarkers - 1; i > -1; i--)
                {
                    Circle inVehiclePointMarker = inVehiclePointMarkers.remove(i);
                    inVehiclePointMarker.remove();
                }
            }
            List<PathsenseInVehicleLocation> points = inVehicleLocation.getPoints();
            int numPoints = points != null ? points.size() : 0;
            if (numPoints > 0)
            {
                for (int i = 0; i < numPoints; i++)
                {
                    PathsenseInVehicleLocation point = points.get(i);
                    inVehicleLocations.add(0, point);
                    //
                    Circle inVehiclePointMarker = map.addCircle((new CircleOptions()).center(new LatLng(point.getLatitude(), point.getLongitude())).fillColor(Color.BLACK).strokeColor(Color.BLACK).strokeWidth(5).radius(5));
                    inVehiclePointMarkers.add(inVehiclePointMarker);
                }
            }
            numInVehicleLocations = inVehicleLocations.size();
            //
            if (numInVehicleLocations > 1)
            {
                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.width(15);
                polylineOptions.color(Color.BLUE);
                polylineOptions.geodesic(true);
                for (int i = 0; i < numInVehicleLocations; i++)
                {
                    PathsenseInVehicleLocation q_inVehicleLocation = inVehicleLocations.get(i);
                    polylineOptions.add(new LatLng(q_inVehicleLocation.getLatitude(), q_inVehicleLocation.getLongitude()));
                }
                if (mPolylineInVehicle != null)
                {
                    mPolylineInVehicle.remove();
                }
                mPolylineInVehicle = map.addPolyline(polylineOptions);
            }
        }
    }
    boolean isStarted()
    {
        final SharedPreferences preferences = mPreferences;
        //
        if (preferences != null)
        {
            return preferences.getInt("startedFlag", 0) == 1;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sl=this;
        context=this;
        //
        mPreferences = getSharedPreferences("PathsenseInVehicleLocationDemoPreferences", MODE_PRIVATE);
        // receivers


        // Obtain the MapFragment and set the async listener to be notified when the map is ready.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //
        mCreateFlag = 1;


        makePage();
    }

    private void makePage() {
        imgNavigation = (ImageView) findViewById(R.id.btnNavigation);
        btnChat = (ImageView) findViewById(R.id.btnChat);
        btnRoute = (ImageView) findViewById(R.id.btnRoute);
        btnSetting = (ImageView) findViewById(R.id.btnSetting);
        sliderview  = (LinearLayout) findViewById(R.id.sliderview);
        btnHide = (ImageView) findViewById(R.id.btnhide);
        btnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sliderview.setVisibility(View.GONE);
            }
        });
        dialogNavigation  = (CardView) findViewById(R.id.dialogNavigation);
        dialogNavigation.setVisibility(View.GONE);
        btnPassanger = (ImageView) findViewById(R.id.btnPassanger);
        imgNavigation.setOnClickListener(this);
        btnPassanger.setOnClickListener(this);
        btnRoute.setOnClickListener(this);

        btnChat.setOnClickListener(this);
        btnSetting.setOnClickListener(this);

        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رحسین توکلی","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("احمد توکلی","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("محسن رفیق دوست","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));

        sliderobj =new Slider(MainActivity.this,passangerlist);
        sliderobj.run();
        sliderview.setVisibility(View.GONE);
      /*  RecyclerView listview1 = new RecyclerView(context);
        pages.add(listview1);



        viewPager = (ViewPager) findViewById(R.id.viewpager);
        CustomPagerAdapter adapter = new CustomPagerAdapter(context,pages);
        viewPager.setAdapter(adapter);

        listview1.setAdapter(new PassangerAdapter(passangerlist,context));
*/

    }

    @Override
    public void onLocationChanged(Location location)
    {
        final GoogleMap map = mMap;
        //
        if (map != null)
        {
            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
/*
            map.moveCamera(CameraUpdateFactory.newCameraPosition((new CameraPosition.Builder()).target(position).zoom(18).build()));
*/
            // initialize markers

         /*   mGoogleMap.addMarker(new MarkerOptions()
                    .position(mDummyLatLng)
                    .));*/
            mMarkerGroundTruth = mMap.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location_icon)).anchor(.5f, .5f).position(position).title("GroundTruth"));



            LatLng latLng = new LatLng(35.6759083,51.3784215);
            LatLng latLng1 = new LatLng(35.6795599,51.3948044);
            LatLng latLng2 = new LatLng(35.7091693,51.388726);
            LatLng latLng3 = new LatLng(35.6741089,51.3682436);
           mMap.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.profile)))
                    .anchor(.5f, .5f).position(latLng).title("point1"));

            mMap.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.profile)))
                    .anchor(.5f, .5f).position(latLng1).title("point2"));

            mMap.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.profile)))
                    .anchor(.5f, .5f).position(latLng2).title("point3"));

            Drawable circleDrawable = getResources().getDrawable(R.drawable.school_icon1);
            BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

            mMap.addMarker(new MarkerOptions()
                    .position(latLng3)
                    .title("allireza school")
                    .icon(markerIcon)
            );



            CircleOptions circle=new CircleOptions();
            circle.center(latLng3).fillColor(getResources().getColor(R.color.semiPrimaryDark)).strokeColor(getResources().getColor(R.color.semiPrimaryDark)).radius(50);
            mMap.addCircle(circle);

          /*  mMarkerGroundTruth = mMap.addMarker((new MarkerOptions()).icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.profile)))
                    .anchor(.5f, .5f).position(position).title("GroundTruth"));*/
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(position.latitude, position.longitude)).zoom(17.0f).build()));
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    if(marker.getTitle().equals("point1")||marker.getTitle().equals("point2")||marker.getTitle().equals("point3")){

                        sliderview.setVisibility(View.VISIBLE);
                    }



                    return false;
                }
            });
            startUpdates();




/*
            mapRipple = new MapRipple(mMap, latLng3, context);


            mapRipple.withNumberOfRipples(3);
            mapRipple.withFillColor(Color.BLUE);
            mapRipple.withStrokeColor(Color.BLACK);
            mapRipple.withStrokewidth(10);      // 10dp
            mapRipple.withDistance(2000);      // 2000 metres radius
            mapRipple.withRippleDuration(12000);    //12000ms
            mapRipple.withTransparency(0.5f);*/



        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        FusedLocationManager.getInstance(this).requestLocationUpdate(this);
        startService(new Intent(MainActivity.this,mySenderService.class));
     //   startService(new Intent(MainActivity.this,mySenderTwo.class));
        Intent startIntent = new Intent(MainActivity.this, mySenderTwo.class);
        startIntent.setAction("start");
        startService(startIntent);
    /*    Intent startIntent = new Intent(MainActivity.this, PathsenseInVehicleLocationUpdateRunnerService.class);
        startIntent.setAction("start");
        startService(startIntent);*/

    }
    @Override
    protected void onPause()
    {
        super.onPause();
        //
        if (isStarted())
        {
            stopUpdates();
        }
    }
    @Override
    public void onProviderDisabled(String s)
    {
    }
    @Override
    public void onProviderEnabled(String s)
    {
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        //
        if (mCreateFlag == 0)
        {
            startUpdates();
        }
        mCreateFlag = 0;
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle)
    {
    }
    void removePolylineGroundTruth()
    {
        final List<Location> groundTruthLocations = mGroundTruthLocations;
        //
        if (groundTruthLocations != null)
        {
            int numGroundTruthLocations = groundTruthLocations.size();
            if (numGroundTruthLocations > 0)
            {
                for (int i = numGroundTruthLocations - 1; i > -1; i--)
                {
                    groundTruthLocations.remove(i);
                }
            }
            if (mPolylineGroundTruth != null)
            {
                mPolylineGroundTruth.remove();
                mPolylineGroundTruth = null;
            }
        }
    }
    void removePolylineInVehicle()
    {
        final List<PathsenseInVehicleLocation> inVehicleLocations = mInVehicleLocations;
        final List<Circle> inVehiclePointMarkers = mInVehiclePointMarkers;
        //
        if (inVehicleLocations != null && inVehiclePointMarkers != null)
        {
            int numInVehicleLocations = inVehicleLocations.size();
            if (numInVehicleLocations > 0)
            {
                for (int i = numInVehicleLocations - 1; i > -1; i--)
                {
                    inVehicleLocations.remove(i);
                }
            }
            int numInVehiclePointMarkers = inVehiclePointMarkers.size();
            if (numInVehiclePointMarkers > 0)
            {
                for (int i = numInVehiclePointMarkers - 1; i > -1; i--)
                {
                    Circle inVehiclePointMarker = inVehiclePointMarkers.remove(i);
                    inVehiclePointMarker.remove();
                }

            }
            if (mPolylineInVehicle != null)
            {
                mPolylineInVehicle.remove();
                mPolylineInVehicle = null;
            }
        }
    }
    void startUpdates()
    {
            // cleanup
            removePolylineInVehicle();
            removePolylineGroundTruth();
            // register for updates
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
            if (mGroundTruthLocationUpdateReceiver == null)
            {
                mGroundTruthLocationUpdateReceiver = new InternalGroundTruthLocationUpdateReceiver(this);
            }
            localBroadcastManager.registerReceiver(mGroundTruthLocationUpdateReceiver, new IntentFilter("groundTruthLocationUpdate"));
            if (mInVehicleLocationUpdateReceiver == null)
            {
                mInVehicleLocationUpdateReceiver = new InternalInVehicleLocationUpdateReceiver(this);
            }
            localBroadcastManager.registerReceiver(mInVehicleLocationUpdateReceiver, new IntentFilter("inVehicleLocationUpdate"));

    }
    void stopUpdates()
    {
        //
            // unregister for updates
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
            if (mGroundTruthLocationUpdateReceiver != null)
            {
                localBroadcastManager.unregisterReceiver(mGroundTruthLocationUpdateReceiver);
            }
            if (mInVehicleLocationUpdateReceiver != null)
            {
                localBroadcastManager.unregisterReceiver(mInVehicleLocationUpdateReceiver);
            }
            // set start button
            Log.e("startstop","start");
/*
            buttonStart.setText("Start");
*/
    }

    @Override
    protected void onStart() {
        bus.register(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    double unwrapHeading(double heading1, double heading2)
    {
        while (heading1 >= heading2 + 180)
        {
            heading1 -= 360;
        }
        while (heading1 < heading2 - 180)
        {
            heading1 += 360;
        }
        return heading1;
    }


    public void onEvent(MessengerBus event) {
        JSONObject pio = new JSONObject();
        JSONArray arr = new JSONArray();


        try{
        pio.put("lat", event.getLocation().getLatitude());
        pio.put("lon", event.getLocation().getLongitude());
        pio.put("speed", event.getLocation().getSpeed());
            pio.put("time", event.getLocation().getTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        arr.put(pio);
        callerService.sendPoint(sl,arr);
        if (event.getMessage().equals("locchange")) {
            counter++;
           Toast.makeText(context,counter+"",Toast.LENGTH_SHORT).show();
        }
    }


    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


}
