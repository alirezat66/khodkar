package com.khodkar.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.khodkar.R;
import com.khodkar.adapter.PassangerAdapter;
import com.khodkar.objects.Passanger;

import java.util.ArrayList;

/**
 * Created by alireza on 6/17/2017.
 */

public class PassangerActivity extends AppCompatActivity {
    RecyclerView list;
    PassangerAdapter adapter;
    ArrayList<Passanger>passangerlist=new ArrayList<>();
    Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasanger);
        context=this;
        makePage();
    }

    private void makePage() {
        list = (RecyclerView) findViewById(R.id.list);
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رحسین توکلی","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("احمد توکلی","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("محسن رفیق دوست","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));
        passangerlist.add(new Passanger("رضا تقی زاده","خیابان 8 شرقی پلاک 21"));

        adapter = new PassangerAdapter(passangerlist,context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        list.setLayoutManager(mLayoutManager);
        list.setItemAnimator(new DefaultItemAnimator());
        list.setAdapter(adapter);

    }
}
