package com.khodkar.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.khodkar.R;
import com.khodkar.adapter.PassangerAdapter;
import com.khodkar.adapter.SettingAdapter;
import com.khodkar.objects.SettingObject;

import java.util.ArrayList;

/**
 * Created by user16 on 7/11/2017.
 */

public class SettingActivity extends AppCompatActivity {

    RecyclerView list;
    ArrayList<SettingObject> setObjs=new ArrayList<>();
    SettingAdapter adapter  ;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        list  = (RecyclerView) findViewById(R.id.list);

        SettingObject set1 = new SettingObject("ویرایش اطلاعات","اطلاعات هویتی و رمز عبور و..",R.drawable.settings_editprofile_icon,1);
        SettingObject set2 = new SettingObject("مدیریت پرداخت ها","پرداخت هزینه های سرویس، پیگیری و گزارشات مالی",R.drawable.settings_finance_icon,0);
        SettingObject set3 = new SettingObject("مدیریت ارتباطات","مدیریت افرادی که به موقعیت شما دسترسی دارند و دسترسی ها شما به سایر افراد",R.drawable.settings_access_icon,3);
        SettingObject set4 = new SettingObject("خروج از حساب کاربری","حساب فعلی: ۲۹۸۰۰۱۲۱۰۶",R.drawable.settings_exit_icon,0);
        setObjs.add(set1);
        setObjs.add(set2);
        setObjs.add(set3);
        setObjs.add(set4);

        adapter = new SettingAdapter(setObjs,SettingActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        list.setLayoutManager(mLayoutManager);
        list.setItemAnimator(new DefaultItemAnimator());
        list.setAdapter(adapter);

    }
}
