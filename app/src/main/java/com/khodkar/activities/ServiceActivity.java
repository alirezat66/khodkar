package com.khodkar.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.khodkar.R;
import com.khodkar.fragment.HistoryFragment;
import com.khodkar.fragment.MyServicesFragment;

import goldzweigapps.tabs.Builder.EasyTabsBuilder;
import goldzweigapps.tabs.Colors.EasyTabsColors;
import goldzweigapps.tabs.Items.TabItem;
import goldzweigapps.tabs.View.EasyTabs;

/**
 * Created by alireza on 7/9/2017.
 */

public class ServiceActivity extends AppCompatActivity {
    EasyTabs tabs ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        defineViews();
    }

    private void defineViews() {
         tabs= (EasyTabs) findViewById(R.id.EasyTabs);



        EasyTabsBuilder.with(tabs).addTabs(

                new TabItem(new MyServicesFragment(), "سرویس های من"), // Add four Tab items with fragment and title
                new TabItem(new HistoryFragment(), "تاریخچه")
        ).setTabsBackgroundColor(EasyTabsColors.White)
                .setIndicatorColor(getResources().getColor(R.color.purpleheart))
                .setTextColors(getResources().getColor(R.color.purpleheart),getResources()
                .getColor(R.color.languidlavender)) //Setting two colors selected one and unselected one
                .setTabLayoutScrollable(false)
                .setCustomTypeface(Typeface.createFromAsset(getAssets(), "IRANSansMobile.ttf"))
                .changeIconPosition("left")

                .Build();
    }
}
