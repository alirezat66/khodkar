package com.khodkar.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.Button;

import com.khodkar.helper.Utility;


/**
 * Created by ahmad on 1/16/17.
 */

public class CustomCurveButton extends android.support.v7.widget.AppCompatButton {

    public CustomCurveButton(Context context) {
        super(context);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomCurveButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomCurveButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init();
        }
    }



    private void init() {

//        setTypeface(Utility.getRegularTypeFace());

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(15);
        ColorDrawable buttonColor = (ColorDrawable) getBackground();
        shape.setColor(buttonColor.getColor());
        setBackground(shape);

    }
}
