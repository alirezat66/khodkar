package com.khodkar.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khodkar.R;


public class ConfirmationDialog extends Dialog {
   // String acc;
    Context context;
    DialogListener listener;
    LinearLayout lyNext,lyReport;
    /* String deny;
    TextView dlg_noBtn;
    TextView dlg_text;
    TextView dlg_title;
    TextView dlg_yesBtn;
    boolean isNormal = true;
    String text;
    String title;
*/
    public DialogListener getListener() {
        return this.listener;
    }

    public void setListener(DialogListener listener) {
        this.listener = listener;
    }


    public ConfirmationDialog(Context context) {
        super(context);
        this.context = context;
    }

    public ConfirmationDialog(Context context, String title, String text, String acc, String deny) {
        super(context);
        this.context = context;

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.dialog_confirmation);
        setCancelable(false);
        LayoutParams params = getWindow().getAttributes();
        params.width = -1;
        getWindow().setAttributes(params);
        this.lyNext = (LinearLayout) findViewById(R.id.ly_next);
        this.lyReport = (LinearLayout) findViewById(R.id.ly_report);

        this.lyNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.rejected();
            }
        });
        this.lyReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.rejected();
            }
        });
    }
}
