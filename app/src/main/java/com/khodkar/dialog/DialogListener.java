package com.khodkar.dialog;

public interface DialogListener {
    void accepted();

    void rejected();
}
