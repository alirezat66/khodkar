package com.khodkar.retrofit;

import com.google.gson.JsonObject;

import org.json.JSONArray;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface reqInterface {
    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> About(@Field("action") String str, @Field("key") String str2, @Field("type") String str3);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> ChangePass(@Field("action") String str, @Field("key") String str2, @Field("type") String str3, @Field("step") String str4, @Field("userid") String str5, @Field("password") String str6);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> DeleteMessage(@Field("action") String str, @Field("key") String str2, @Field("step") String str3, @Field("userid") String str4, @Field("id") String str5);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> DriverProfile(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> EndTrip(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3, @Field("step") String str4, @Field("type") String str5, @Field("tripid") String str6, @Field("lat") String str7, @Field("lon") String str8, @Field("speed") String str9);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> Messages(@Field("action") String str, @Field("key") String str2, @Field("type") String str3, @Field("step") String str4, @Field("userid") String str5);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> News(@Field("action") String str, @Field("key") String str2, @Field("type") String str3, @Field("step") String str4, @Field("userid") String str5);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> SaveOrder(@Field("action") String str, @Field("key") String str2, @Field("step") String str3, @Field("data") String str4, @Field("userid") String str5);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> SendMessage(@Field("action") String str, @Field("key") String str2, @Field("message") String str3, @Field("title") String str4, @Field("step") String str5, @Field("userid") String str6);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> StartTrip(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3, @Field("step") String str4, @Field("type") String str5, @Field("lat") String str6, @Field("lon") String str7, @Field("speed") String str8);

    @POST("./")
    @Multipart
    Call<JsonObject> UploadImage(@Part("userid") RequestBody requestBody, @Part("action") RequestBody requestBody2, @Part("key") RequestBody requestBody3, @Part("step") RequestBody requestBody4, @Part("tripid") RequestBody requestBody5, @Part MultipartBody.Part part);

    @POST("GetAppLastVer_Android")
    Call<JsonObject> VersionControl();

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> getStudent(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3, @Field("step") String str4);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> login(@Field("key") String str, @Field("action") String str2, @Field("type") String str3, @Field("mobile") String str4, @Field("password") String str5);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> sendPoint(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3, @Field("data") JSONArray jSONArray, @Field("tripid") String str4);

    @FormUrlEncoded
    @POST("./")
    Call<JsonObject> setTrip(@Field("userid") String str, @Field("action") String str2, @Field("key") String str3, @Field("step") String str4, @Field("studentid") String str5, @Field("tripid") String str6, @Field("type") String str7, @Field("lat") String str8, @Field("lon") String str9);
}
