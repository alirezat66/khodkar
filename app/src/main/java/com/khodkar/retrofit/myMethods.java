package com.khodkar.retrofit;

public enum myMethods {
    Authentication("Authentication", 0),
    SavePoi("SaveP", 1),
    Student("Student", 2),
    Trip("Trip", 3),
    TripStart("TripStart", 4),
    TripENd("TripEnd", 5),
    Upload("Upload", 6),
    Driver("Driver", 7),
    About("About", 8),
    News("News", 9),
    SendMessage("SendMessage", 10),
    ChangePass("ChangePass", 11),
    MessageList("MessageList", 12),
    DeleteMessages("DeleteMessage", 13),
    SaveOrder("SaveOrder", 14);
    
    private String methodName;
    private int methodValue;

    public int getMethodValue() {
        return this.methodValue;
    }

    private myMethods(String toString, int value) {
        this.methodName = toString;
        this.methodValue = value;
    }

    public String toString() {
        return this.methodName;
    }
}
