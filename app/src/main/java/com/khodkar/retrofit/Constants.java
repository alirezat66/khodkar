package com.khodkar.retrofit;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.khodkar.Controller.AppController;


public class Constants {
    public static String Key = "vrs@#R#$_0^!*F@#DS#";
    public static String URL_API = "http://www.varasservice.ir/service/index.php/";
    public static String token = "";

    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) AppController.getCurrentActivity().getSystemService("input_method");
        View view = AppController.getCurrentActivity().getCurrentFocus();
        if (view == null) {
            view = new View(AppController.getCurrentActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
