package com.khodkar.retrofit;

import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServerResponse {
    public void setCall(final myMethods method, Call<JsonObject> call, final ServerListener serverListener) {
        Log.i("==UrlServer", call.request().url().toString());
        call.enqueue(new Callback<JsonObject>() {
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION) {
                    Log.i("==GetServer", ((JsonObject) response.body()).toString());
                    try {
                        JsonObject data = (JsonObject) response.body();
                        if (data != null) {
                            serverListener.onSuccess(method.getMethodValue(), data);
                            return;
                        } else {
                            serverListener.onFailure(method.getMethodValue(), serverErrors.NullError.getErrorName());
                            return;
                        }
                    } catch (Exception e) {
                        serverListener.onFailure(method.getMethodValue(), serverErrors.NotJsaonResponse.getErrorName());
                        return;
                    }
                }
                serverListener.onFailure(method.getMethodValue(), serverErrors.ServerError.getErrorName());
            }

            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t != null && t.getMessage() != null) {
                    Log.e("=====", t.getMessage());
                    serverListener.onFailure(method.getMethodValue(), serverErrors.ServerError.getErrorName());
                }
            }
        });
    }
}
