package com.khodkar.retrofit;

import com.khodkar.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            if (BuildConfig.DEBUG) {
                retrofit = new Builder().baseUrl(Constants.URL_API).addConverterFactory(GsonConverterFactory.create()).client(new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                    //    ongoing.addHeader(HttpRequest.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
                        return chain.proceed(ongoing.build());
                    }
                }).connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).build()).build();
            } else {
                retrofit = new Builder().baseUrl(Constants.URL_API).addConverterFactory(GsonConverterFactory.create()).client(new OkHttpClient.Builder().addInterceptor(new Interceptor() {
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                  //      ongoing.addHeader(HttpRequest.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
                        return chain.proceed(ongoing.build());
                    }
                }).connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).build()).build();
            }
        }
        return retrofit;
    }
}
