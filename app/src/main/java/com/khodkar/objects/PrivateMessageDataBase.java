package com.khodkar.objects;

public class PrivateMessageDataBase {
    public int id;
    public String mtype; // mtype = text || file || photo || voice || video || location || sticker
    public String photo;
    public long timestamp;
    public String events;
    public String text;


    public PrivateMessageDataBase(int id, String mtype, String photo, long timestamp, String events, String text) {
        this.id = id;
        this.mtype = mtype;
        this.photo = photo;
        this.timestamp = timestamp;
        this.events = events;
        this.text = text;
    }


    public PrivateMessageDataBase(int id,String mtype, String text) {
        this.mtype = mtype;
        this.id=id;
        this.text = text;
    }


    public int getId() {
        return id;
    }

    public String getMtype() {
        return mtype;
    }

    public String getPhoto() {
        return photo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getEvents() {
        return events;
    }

    public String getText() {
        return text;
    }
}
