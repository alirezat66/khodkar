package com.khodkar.objects;

/**
 * Created by alireza on 6/17/2017.
 */

public class Passanger {
    String name;
    String address;
    boolean openContact=false;
    boolean isExit=false;
    boolean inEnter=false;
    double lat;
    double  lang;

    public double getLat() {
        return lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLat(double lat) {

        this.lat = lat;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public void setExit(boolean exit) {
        isExit = exit;
    }

    public void setInEnter(boolean inEnter) {
        this.inEnter = inEnter;
    }

    public Passanger(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public boolean isExit() {
        return isExit;
    }

    public boolean isInEnter() {
        return inEnter;
    }

    public boolean isOpenContact() {
        return openContact;
    }

    public void setOpenContact(boolean openContact) {
        this.openContact = openContact;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
