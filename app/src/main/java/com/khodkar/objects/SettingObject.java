package com.khodkar.objects;

/**
 * Created by user16 on 7/11/2017.
 */

public class SettingObject {
    String title;
    String desc;
    int picId;
    int number;
    Class activityClass;

    public SettingObject(String title, String desc, int picId, int number) {
        this.title = title;
        this.desc = desc;
        this.picId = picId;
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public int getPicId() {
        return picId;
    }

    public int getNumber() {
        return number;
    }

    public Class getActivityClass() {
        return activityClass;
    }
}
